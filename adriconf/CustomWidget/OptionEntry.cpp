#include "OptionEntry.h"

OptionEntry::OptionEntry(const ProfileOption_ptr& optionPtr)
    : optionPtr(optionPtr) {
    this->set_visible(true);
    this->set_text(optionPtr->getValue());

    this->signal_changed().connect(
        sigc::mem_fun(*this, &OptionEntry::onTextChange));
}

void OptionEntry::onTextChange() {
    this->optionPtr->setValue(this->get_text());
}
