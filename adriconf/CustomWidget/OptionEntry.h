#ifndef ADRICONF_OPTIONENTRY_H
#define ADRICONF_OPTIONENTRY_H

#include "gtkmm.h"
#include "../ValueObject/ProfileOption.h"

class OptionEntry : public Gtk::Entry {
private:
    ProfileOption_ptr optionPtr;

public:
    explicit OptionEntry(const ProfileOption_ptr &optionPtr);

    void onTextChange();
};

#endif //ADRICONF_OPTIONENTRY_H