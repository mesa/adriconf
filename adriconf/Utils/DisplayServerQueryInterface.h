#ifndef ADRICONF_DISPLAYSERVERQUERYINTERFACE_H
#define ADRICONF_DISPLAYSERVERQUERYINTERFACE_H

#include "../ValueObject/DriverConfiguration.h"

#include <glibmm/ustring.h>
#include <list>

class DisplayServerQueryInterface {
    public:
    virtual std::list<DriverConfiguration_ptr> queryDriverConfigurationOptions()
        = 0;
    virtual bool checkNecessaryExtensions() = 0;
    virtual ~DisplayServerQueryInterface()  = default;
};

#endif // ADRICONF_DISPLAYSERVERQUERYINTERFACE_H