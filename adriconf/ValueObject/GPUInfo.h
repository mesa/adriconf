#ifndef ADRICONF_GPUINFO_H
#define ADRICONF_GPUINFO_H

#include "Profile.h"
#include "Section.h"

#include <glibmm/ustring.h>
#include <map>
#include <memory>

class GPUInfo {
    private:
    Glib::ustring pciId;
    Glib::ustring driverName;
    Glib::ustring deviceName;
    Glib::ustring vendorName;
    uint16_t vendorId;
    uint16_t deviceId;
    std::list<Section_ptr> sections;

    public:
    [[nodiscard]] const Glib::ustring& getPciId() const;

    void setPciId(const Glib::ustring& pciId);

    [[nodiscard]] const Glib::ustring& getDriverName() const;

    void setDriverName(const Glib::ustring& driverName);

    [[nodiscard]] const Glib::ustring& getDeviceName() const;

    void setDeviceName(const Glib::ustring& deviceName);

    [[nodiscard]] const Glib::ustring& getVendorName() const;

    void setVendorName(const Glib::ustring& vendorName);

    [[nodiscard]] uint16_t getVendorId() const;

    void setVendorId(uint16_t vendorId);

    [[nodiscard]] uint16_t getDeviceId() const;

    void setDeviceId(uint16_t deviceId);

    bool operator==(const GPUInfo& rhs);

    [[nodiscard]] const std::list<Section_ptr>& getSections() const;

    void setSections(const std::list<Section_ptr>& newSections);

    [[nodiscard]] std::map<Glib::ustring, Glib::ustring> getOptionsMap() const;

    /* Sort the options inside each section to be more user-friendly */
    void sortSectionOptions();

    /* Generate a new application based on this driver-supported options */
    [[nodiscard]] Profile_ptr generateApplication() const;
};

typedef std::shared_ptr<GPUInfo> GPUInfo_ptr;

#endif
