#ifndef ADRICONF_PROFILEOPTION_H
#define ADRICONF_PROFILEOPTION_H

#include <glibmm/ustring.h>
#include <memory>

class ProfileOption {
private:
    Glib::ustring name;
    Glib::ustring value;

public:
    const Glib::ustring &getName() const;

    void setName(Glib::ustring name);

    const Glib::ustring &getValue() const;

    void setValue(Glib::ustring value);
};

typedef std::shared_ptr<ProfileOption> ProfileOption_ptr;

#endif
