#ifndef DRICONF3_DEVICE_H
#define DRICONF3_DEVICE_H

#include "Profile.h"

#include <glibmm/ustring.h>
#include <list>
#include <memory>

class Device {
    private:
    Glib::ustring driver;
    int screen;
    std::list<Profile_ptr> applications;

    public:
    const Glib::ustring& getDriver() const;

    void setDriver(Glib::ustring driver);

    const int& getScreen() const;

    void setScreen(int screen);

    std::list<Profile_ptr>& getApplications();

    const std::list<Profile_ptr>& getApplications() const;

    void addApplication(Profile_ptr application);

    Profile_ptr findApplication(const Profile_ptr& other) const;

    void sortApplications();

    Device();
};

typedef std::shared_ptr<Device> Device_ptr;

#endif
