#include "Section.h"

Section::Section()
    : options() { }

const Glib::ustring& Section::getDescription() const {
    return this->description;
}

const std::list<DriverOption_ptr>& Section::getOptions() const {
    return this->options;
}

Section* Section::setDescription(Glib::ustring newDescription) {
    this->description = std::move(newDescription);

    return this;
}

Section* Section::addOption(const DriverOption_ptr& option) {
    this->options.push_back(option);

    return this;
}

void Section::sortOptions() {
    this->options.sort(
        [](const DriverOption_ptr& a, const DriverOption_ptr& b) {
            return a->getSortValue() < b->getSortValue();
        });
}
