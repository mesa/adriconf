#include "../adriconf/ValueObject/Profile.h"
#include "gtest/gtest.h"

class ProfileTest : public ::testing::Test {
    public :
    Profile_ptr p;

    ProfileTest() {
        p = std::make_shared<Profile>();
    }
};

TEST_F(ProfileTest, isDefaultApp) {
    ASSERT_TRUE(p->isDefaultApp());

    p->setExecutableRegex("something");
    ASSERT_FALSE(p->isDefaultApp());

    p->setExecutableRegex("");
    p->setExecutable("other");
    ASSERT_FALSE(p->isDefaultApp());

    p->setExecutable("");
    p->setSha1("352453453454");
    ASSERT_FALSE(p->isDefaultApp());
}

TEST_F(ProfileTest, isSameExecutable) {
    auto p2 = std::make_shared<Profile>();

    ASSERT_TRUE(p->isSameExecutable(p2));

    p->setExecutable("something");
    ASSERT_FALSE(p->isSameExecutable(p2));

    p2->setExecutable("something");
    ASSERT_TRUE(p->isSameExecutable(p2));

    p->setExecutableRegex("[a-Z]");
    ASSERT_FALSE(p->isSameExecutable(p2));

    p2->setExecutableRegex("[a-Z]");
    ASSERT_TRUE(p->isSameExecutable(p2));

    p->setSha1("321354151");
    p2->setSha1("bazinga");
    ASSERT_FALSE(p->isSameExecutable(p2));

    p2->setSha1("321354151");
    ASSERT_TRUE(p->isSameExecutable(p2));
}